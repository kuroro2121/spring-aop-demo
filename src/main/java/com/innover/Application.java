package com.innover;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication

public class Application {

	public static void main(String[] args) throws NoSuchMethodException, ClassNotFoundException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

//		Test.Case1();
		Test bean = context.getBean(Test.class);
		bean.Case1();
	}
}
